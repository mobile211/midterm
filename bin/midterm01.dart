import 'dart:io';
import 'dart:math';
import 'package:midterm01/midterm01.dart' as midterm01;

void main(List<String> arguments) {
  print('Input mathematics expressions: ');
  String mathEx = stdin.readLineSync()!;
  List<dynamic> infix = infixProcess(mathEx);
  print(infix);
  List<dynamic> postfix = postfixProcess(infix);
  print(postfix);
  List<dynamic> values = evaPostfixProcess(postfix);
  print(values);
}

List<dynamic> evaPostfixProcess(List<dynamic> postfix) {
  print('Evaluate Postfix: ');
  var values = [];
  var left = [];
  var right = [];

  for (int i = 0; i < postfix.length; i++) {
    if (isNumeric(postfix[i])) {
      values.add(int.parse(postfix[i]));
    } else {
      right.add(values.removeAt(values.length - 1));
      left.add(values.removeAt(values.length - 1));
      if (postfix[i] == '+') {
        values.add(left[0] + right[0]);
        left.removeAt(0);
        right.removeAt(0);
      }
      if (postfix[i] == '-') {
        values.add(left[0] - right[0]);
        left.removeAt(0);
        right.removeAt(0);
      }
      if (postfix[i] == '*') {
        values.add(left[0] * right[0]);
        left.removeAt(0);
        right.removeAt(0);
      }
      if (postfix[i] == '/') {
        values.add(left[0] / right[0]);
        left.removeAt(0);
        right.removeAt(0);
      }
      if (postfix[i] == '^') {
        values.add(pow(left[0], right[0]));
        left.removeAt(0);
        right.removeAt(0);
      }
    }
  }
  return values;
}

List<dynamic> postfixProcess(List<dynamic> infix) {
  var postfix = [];
  var oper = [];
  print('Postfix: ');
  for (int i = 0; i < infix.length; i++) {
    if (isNumeric(infix[i])) {
      postfix.add(infix[i]);
    }
    if (infix[i] == '+' ||
        infix[i] == '-' ||
        infix[i] == '*' ||
        infix[i] == '/' ||
        infix[i] == '^') {
      while (oper.isNotEmpty &&
          oper[oper.length - 1] != '(' &&
          checkOper(infix[i]) < checkOper(oper[oper.length - 1])) {
        postfix.add(oper.removeAt(oper.length - 1));
      }
      oper.add(infix[i]);
    }
    if (infix[i] == '(') {
      oper.add(infix[i]);
    }
    if (infix[i] == ')') {
      while (oper[oper.length - 1] != '(') {
        postfix.add(oper.removeAt(oper.length - 1));
      }
      oper.removeAt(oper.length - 1);
    }
  }

  while (oper.isNotEmpty) {
    postfix.add(oper.removeAt(oper.length - 1));
  }
  return postfix;
}

int checkOper(String x) {
  if (x == '^') {
    return 3;
  } else if (x == '*' || x == '/') {
    return 2;
  } else if (x == '+' || x == '-') {
    return 1;
  }
  return -1;
}

bool isNumeric(String s) {
  if (s == null) {
    return false;
  }
  return double.tryParse(s) != null;
}

List<dynamic> infixProcess(String mathEx) {
  var infix = [];
  print('Infix: ');
  String numText = '';
  for (int i = 0; i < mathEx.length; i++) {
    if (isNumeric(mathEx[i])) {
      numText = numText + mathEx[i];
    } else if (!isNumeric(mathEx[i])) {
      if (numText != '') {
        infix.add(numText);
        numText = '';
      }
      if (mathEx[i] == '+' ||
          mathEx[i] == '-' ||
          mathEx[i] == '*' ||
          mathEx[i] == '/' ||
          mathEx[i] == '^' ||
          mathEx[i] == '(' ||
          mathEx[i] == ')') {
        infix.add(mathEx[i]);
      }
      if (mathEx[i] == ' ') {}
    }
    if (i == mathEx.length - 1) {
      infix.add(numText);
    }
  }
  return infix;
}
